#!/usr/bin/python3
from setuptools import setup

with open('README.md','r') as f:
    long_description = f.read()

setup(
    name='pygameqt',
    version='0.1.0',
    author='(Lis)anne Mocha',
    maintainer='(Lis)anne Mocha',
    packages=['pygameqt'],
    url='https://gitlab.com/mocchapi/pygameqt',
    license='GPLv3',
    description='Tiny package for integrating pygame surfaces into PyQT5',
    long_description=long_description,
    long_description_content_type='text/markdown',
    install_requires=[
        "pyqt5",
        "pygame",
    ],
    classifiers=[
        'Intended Audience :: Developers',
        'Environment :: X11 Applications :: Qt',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3 :: Only'
    ],
    keywords=[
        'pygame',
        'pyqt',
        'pyqt5'
        'pygameqt'
    ]
)