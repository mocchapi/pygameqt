import sys
import pygame
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QGridLayout, QSpinBox, QPushButton

from pygameqt import SurfaceWidget


"""A simple example that places a pygame surface with QT controls to change the background color of said surface"""

def main(args=sys.argv):
    pygame.init()
    app = QApplication(args)

    # the pygame surface that gets drawn in the qt window
    surface = pygame.Surface([300,300])
    widget = SurfaceWidget(surface)
    widget.rgb = (0,0,0)

    #dummy parent widget
    p = QWidget()
    layout = QGridLayout(p)

    # create the qt ui elements
    rgb_r = QSpinBox()
    rgb_g = QSpinBox()
    rgb_b = QSpinBox()
    for rgb in [rgb_r, rgb_b, rgb_g]: rgb.setMaximum(255)
    rgb_set = QPushButton()
    rgb_set.setText('Set RGB')

    # add them to the gridlayout
    layout.addWidget(rgb_b, 1, 3, 1, 1)
    layout.addWidget(rgb_g, 1, 2, 1, 1)
    layout.addWidget(rgb_r, 1, 1, 1, 1)
    layout.addWidget(rgb_set, 2, 1, 1, 3)
    layout.addWidget(widget, 0, 1, 1, 3)


    def setsurfaceclr():
        # change the surface color to the rgb values from the spinboxes
        rgb = rgb_r.value(), rgb_g.value(), rgb_b.value()
        widget.getSurface().fill(rgb)

        #important: redraw converts the pygame surface into a qt-readable image object again, without this nothing would change in the window
        widget.redraw()
    
    rgb_set.clicked.connect(setsurfaceclr)

    window = QMainWindow()
    window.setCentralWidget(p)
    window.show()

    sys.exit(app.exec())

if __name__ == '__main__':
    main()