# PygameQT

Tiny package for integrating pygame surfaces into PyQT5

## Installation

1. Install the packages in `requirements.txt` with `python3 -m pip install -r requirements.txt` (On windows: you may have to use `python` or `py`)
2. Install pygameqt
    - Through pypi: use `pip install -U pygameqt`
    - Through git: use `pip install git+https://gitlab.com/mocchapi/pygameqt.git`

## Usage

Usage is pretty straight forward, here's a small example of just displaying a plain pygame surface in a QT window:

```python
import pygame
from PyQt5.QtWidgets import QApplication, QMainWindow
from pygameqt import SurfaceWidget

pygame.init()
app = QApplication([])

surface = pygame.Surface((200,100))
widget = SurfaceWidget(surface)

window = QMainWindow()
window.setCentralWidget(widget)
window.show()
app.exec()
```

Check out the examples directory for more