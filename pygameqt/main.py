from PyQt5.QtWidgets import QWidget
from PyQt5.QtGui import QPainter, QImage

import pygame

# a lot of the code for this has come from https://stackoverflow.com/questions/38280057/how-to-integrate-pygame-and-pyqt4 by Photon (https://stackoverflow.com/users/1648011/photon)
# archive: https://web.archive.org/web/20210827151936/https://stackoverflow.com/questions/38280057/how-to-integrate-pygame-and-pyqt4


class SurfaceWidget(QWidget):
    """The QT QWidget that's used to display the pygame surface in a QT window.

    args:
        surface (pygame.Surface): The pygame surface to display
        parent (QObject): The QT parent, same as any widget
        """
    def __init__(self, parent=None, surface=None):
        super().__init__(parent)
        if surface == None:
            surface = pygame.Surface((200,200))
        self.surface = surface
        self.redraw()
    
    def setSurface(self, surface, redraw = True):
        """Swap out the pygame surface for another. Also accessible through `SurfaceWidget.surface`
        
        args:
            surface (pygame.Surface): The surface to change to
            redraw (bool): If True, calls `SurfaceWidget.redraw()` to update the QT ui"""
        self.surface = surface
        if redraw:
            self.redraw()
    
    def getSurface(self):
        """Returns the pygame surface. Also accessible through `SurfaceWidget.surface`"""
        return self.surface

    def redraw(self):
        """Should be called after any change to the surface, or else it will not update in the QT ui. A good rule of thumb is to call this after you've made all your changes, such as `.blit()`s or `.fill()`s"""
        w,h = self.surface.get_size()
        self.data = self.surface.get_buffer().raw
        self.image = QImage(self.data, w, h, QImage.Format_RGB32)
        self.update()
    
    def paintEvent(self, event):
        paint = QPainter()
        paint.begin(self)
        paint.drawImage(0, 0, self.image)
        paint.end()